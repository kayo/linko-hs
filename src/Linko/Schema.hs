module Linko.Schema where

import Data.Scientific (Scientific)
import Data.Text (Text)

import FRP.Sodium

data Value = NumVal Scientific

type Value t = Value (Behavior t)
