module Linko.Model where

import Data.Default (Default(..))
import Data.Scientific (Scientific)
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as H
import Data.Text (Text)

type Name = Text
type Path = Text

instance Default Bool where
  def = False

instance Default (HashMap k v) where
  def = H.empty

data Role = Role { roleTitle :: Maybe Text
                 , roleDescription :: Maybe Text
                 }
            deriving (Show, Eq)

instance Default Role where
  def = Role Nothing Nothing

data Access = Access { accessGet :: [Name]
                     , accessSet :: [Name]
                     }
            deriving (Show, Eq)

instance Default Access where
  def = Access [] []

data Model = Model { modelTitle :: Maybe Text
                   , modelDescription :: Maybe Text
                   , modelRole :: HashMap Name Role
                   , modelAccess :: HashMap Name Access
                   , modelNode :: HashMap Name Node
                   }
           deriving (Show, Eq)

instance Default Model where
  def = Model Nothing Nothing def def def

data Node = Group { groupTitle :: Maybe Text       -- Title
                  , groupDescription :: Maybe Text -- Description
                  , groupAccess :: Maybe Name      -- Access scheme
                  , groupNode :: HashMap Name Node -- Children nodes
                  }
          | Entry { entryTitle :: Maybe Text       -- Title
                  , entryDescription :: Maybe Text -- Description
                  , entryAccess :: Maybe Name      -- Access scheme
                  , entryUnit :: Maybe Text        -- Units of value
                  , entryType :: Type              -- Type of value
                  , entryGet :: Bool               -- Readable
                  , entrySet :: Bool               -- Writable
                  , entryVal :: Val                -- Value
                  , entryExp :: Maybe Exp          -- Expression
                  }
          deriving (Show, Eq)

instance Default Node where
  def = Entry def def def def def def def def def

type Number = Scientific

data Type = NumType { numDef :: Maybe Number -- Default value
                    , numMin :: Maybe Number -- Minimum value
                    , numMax :: Maybe Number -- Maximum value
                    , numStp :: Maybe Number -- Step value
                    }
          | StrType { strDef :: Maybe Text   -- Default value
                    , strLen :: Maybe Int    -- Maximum length
                    , strPat :: Maybe Text   -- Pattern
                    }
          | BoolType { boolDef :: Maybe Bool -- Default Value
                     }
          | EnumType { enumDef :: Maybe Text   -- Default value
                     , enumVal :: HashMap Text EnumOpt -- Set of values
                     }
          deriving (Show, Eq)

instance Default Type where
  def = NumType Nothing Nothing Nothing Nothing

data EnumOpt = EnumOpt { enumTitle :: Maybe Text
                       , enumDescription :: Maybe Text
                       }
             deriving (Show, Eq)

data Val = NumVal !Number
         | StrVal !Text
         | BoolVal !Bool
         | VoidVal
         deriving (Show, Eq)

toNum :: Val -> Maybe Number
toNum (NumVal v) = Just v
toNum _ = Nothing

fromNum :: Maybe Number -> Val
fromNum (Just v) = NumVal v
fromNum _ = VoidVal

toStr :: Val -> Maybe Text
toStr (StrVal v) = Just v
toStr _ = Nothing

fromStr :: Maybe Text -> Val
fromStr (Just v) = StrVal v
fromStr _ = VoidVal

toBool :: Val -> Maybe Bool
toBool (BoolVal v) = Just v
toBool _ = Nothing

fromBool :: Maybe Bool -> Val
fromBool (Just v) = BoolVal v
fromBool _ = VoidVal

instance Default Val where
  def = VoidVal

data CondCase = CondThen { condWhen :: Exp
                         , condThen :: Exp
                         }
              | CondElse { condElse :: Exp
                         }
              deriving (Show, Eq)

data Op1 = BoolNot
         | NumNeg
         | StrLen
         deriving (Show, Eq)

data OpN = Equal
         | NotEq
         | BoolAnd
         | BoolOr
         | NumAdd
         | NumSub
         | NumMul
         | NumDiv
         | NumLt
         | NumLe
         | NumGt
         | NumGe
         | StrCat
         deriving (Show, Eq)

data Exp = Val Val
         | Node Path
         | Op1 Op1 Exp
         | OpN OpN [Exp]
         | Conds [CondCase]
         deriving (Show, Eq)
