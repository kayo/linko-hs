module Linko.Engine ( ModelSync(..)
                    , MonadEngine(..)
                    , Engine
                    , EngineT
                    , newEngine
                    , runEngine
                    , loadModel
                    , getNode
                    , getValue
                    , setValue
                    , onChange
                    , onValue
                    , offValue
                    , getModel
                    , liftFRP ) where

import Data.Default (Default(..))

import Control.Applicative ((<$>), (<*>), pure)

import Control.Monad.IO.Class (MonadIO(..))
import Control.Monad.Trans.Class (MonadTrans(..))
import Control.Monad.Trans.Reader (ReaderT, runReaderT, ask)
import Control.Monad.Trans.Except (ExceptT, runExceptT, throwE)
import Control.Monad (when, unless)

import Control.Monad.STM (STM, atomically)
import Control.Concurrent.STM.TVar (TVar, newTVar, readTVar, modifyTVar)

import Data.Traversable (sequenceA)
import qualified Data.Text as T
import qualified Data.Scientific as N
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as H
import qualified Data.HashSet as S

import FRP.Sodium

import Linko.Model
import Linko.Model.Process

data ModelSync = ModelSync { modelLoad :: IO (Either String Model)
                           , modelSave :: Model -> IO ()
                           }

data EngineState = EngineState { engineModelSync :: ModelSync
                               , engineModel :: Maybe Model
                               , engineNode :: HashMap Path EngineNode
                               }

data EngineNode = EngineNode { engineValue :: Behavior Val
                             , engineChange :: Maybe (Val -> Reactive ())
                             , engineChanged :: Event Val
                             , engineUnlisten :: HashMap Name (IO ())
                             }

type Engine = TVar EngineState

class (MonadIO m) => MonadEngine m where
  getEngine :: m Engine
  throwError :: String -> m a
  --runEngine :: (MonadEngine m, MonadIO n) => m a -> Engine -> m (Either String a)
  --liftEngine :: (MonadIO n) => n a -> m a

type EngineT m = ExceptT String (ReaderT Engine m)

instance (MonadIO m) => MonadEngine (EngineT m) where
  getEngine = lift ask
  throwError = throwE
  --runEngine m = runReaderT (runExceptT m)
  --liftEngine = lift . lift

runEngine :: (MonadIO m) => EngineT m a -> Engine -> m (Either String a)
runEngine = runReaderT . runExceptT

liftSTM :: (MonadIO m) => STM a -> m a
liftSTM = liftIO . atomically

liftFRP :: (MonadIO m) => Reactive a -> m a
liftFRP = liftIO . sync

newEngine :: (MonadIO m) => ModelSync -> m Engine
newEngine modelSync = liftSTM $ newTVar $ EngineState modelSync def def

getNode :: (MonadEngine m) => Path -> m EngineNode
getNode path = getEngine >>= liftSTM . readTVar >>=
               maybe (throwError $ "Not found: " ++ T.unpack path) return . H.lookup path . engineNode

getValue :: (MonadEngine m) => Path -> m Val
getValue path = getNode path >>= liftFRP . sample . engineValue

setValue :: (MonadEngine m) => Path -> Val -> m ()
setValue path val = getNode path >>=
                    maybe (throwError "Read only") return . engineChange >>=
                    (\set -> liftFRP $ set val)

onChange :: (MonadEngine m) => Path -> (Val -> IO ()) -> m (IO ())
onChange path cb = getNode path >>= liftFRP . flip listen cb . engineChanged

onValue :: (MonadEngine m) => Name -> Path -> (Val -> IO ()) -> m ()
onValue subj path handler = getNode path >>=
                            liftFRP . flip listen handler . engineChanged >>=
                            addUnlisten
  where
    --addUnlisten :: (MonadEngine m) => (IO ()) -> m ()
    addUnlisten unlisten = getEngine >>= liftSTM . flip modifyTVar add
      where
        add engine = engine { engineNode = H.adjust add' path $ engineNode engine }
        add' node = node { engineUnlisten = H.insert subj unlisten $ engineUnlisten node }

offValue :: (MonadEngine m) => Name -> Path -> m ()
offValue subj path = doUnlisten >> delUnlisten
  where
    doUnlisten = getEngine >>= liftSTM . readTVar >>=
                  maybe (return ()) (maybe (return ()) liftIO . H.lookup subj . engineUnlisten) . H.lookup path . engineNode

    --delUnlisten :: (MonadEngine m) => m ()
    delUnlisten = getEngine >>= liftSTM . flip modifyTVar del
      where
        del engine = engine { engineNode = H.adjust del' path $ engineNode engine }
        del' node = node { engineUnlisten = H.delete subj $ engineUnlisten node }

getModel :: (MonadEngine m) => m (Maybe Model)
getModel = getEngine >>= liftSTM . readTVar >>= return . engineModel

loadModel :: (MonadEngine m) => m ()
loadModel = do
  engine <- getEngine
  loader <- liftSTM $ (modelLoad . engineModelSync) <$> readTVar engine
  model <- liftIO loader >>= either throwError return
  
  -- check model
  let deps = buildDeps model
  let res = checkDeps deps
  when (res /= def) $ throwError $ "Dependencies problem: " ++ depsError res
  
  -- build model
  --let newUnlisten = Nothing
  node <- genNodes model deps
  
  liftIO $ putStrLn $ show $ H.keys node
  
  -- reload engine
  liftSTM $ modifyTVar engine $ \e -> e { engineModel = Just model
                                        , engineNode = node }
  
  -- unlisten old listeners
  --maybe (return ()) liftIO mUnlisten

genNodes :: (MonadIO m) => Model -> DepsMap -> m (HashMap Path EngineNode)
genNodes model depmap = gens H.empty $ H.keys depmap
  where
    gens pool [] = return pool
    gens pool (node:rest) =
      let deps = S.toList (depmap H.! node) in
      if node `H.member` pool
      then gens pool rest
      else do
        pool' <- gens pool deps
        pool'' <- case getModelNode model node of
         (Just entry@(Entry {entryVal = val, entryExp = mExp})) -> do
           ent <- liftFRP $ do
             (val, mSetVal) <- if deps == def
                               then buildVal val
                               else buildExp mExp pool'
             return $ EngineNode val mSetVal (value val) def
           return $ H.insert node ent pool'
         _ -> return pool'
        gens pool'' rest
    
    buildVal val = do
      (val, setVal) <- newBehavior val
      return (val, Just setVal)

    buildExp (Just exp) pool = do
      let val = applyExp exp
      return (val, Nothing)
      where
        applyExp :: Exp -> Behavior Val
        applyExp (Val val) = pure val
        applyExp (Node path) = engineValue $ pool H.! path
        applyExp (Op1 op exp) = applyOp1 op <$> applyExp exp
        applyExp (OpN op exps) = applyOpN op <$> sequenceA (map applyExp exps)
        applyExp (Conds cs) = applyConds cs
        
        applyOp1 :: Op1 -> Val -> Val
        applyOp1 BoolNot v = fromBool $ not <$> toBool v
        applyOp1 NumNeg v = fromNum $ ((fromInteger 0) -) <$> toNum v
        applyOp1 StrLen v = fromNum $ fromInteger . toInteger . T.length <$> toStr v
        {-
        applyOpN :: OpN -> [Val] -> Val
        applyOpN Equal = apply
          where
            apply [] = VoidVal
            apply (v:[]) = VoidVal
            apply (v:w:[]) = BoolVal $ v == w
            apply (v:n@(w:_)) = if v /= w then BoolVal False else apply n
        -}
        applyOpN BoolAnd = applyOp2 $ \a b -> fromBool $ (&&) <$> toBool a <*> toBool b
        applyOpN BoolOr = applyOp2 $ \a b -> fromBool $ (||) <$> toBool a <*> toBool b

        applyOpN NumAdd = applyOp2 $ \a b -> fromNum $ (+) <$> toNum a <*> toNum b
        applyOpN NumSub = applyOp2 $ \a b -> fromNum $ (-) <$> toNum a <*> toNum b
        applyOpN NumMul = applyOp2 $ \a b -> fromNum $ (*) <$> toNum a <*> toNum b
        applyOpN NumDiv = applyOp2 $ \a b -> fromNum $ (/) <$> toNum a <*> toNum b

        --applyOpN StrCat = applyOp2 $ \a b -> fromStr $ (T.++) <$> toStr a <*> toStr b
        applyOpN StrCat = apply
          where
            apply s = fromStr $ T.concat <$> sequenceA (map toStr s)
        
        applyOpN Equal = applyOp2' $ \a b -> BoolVal $ a == b
        applyOpN NotEq = applyOp2' $ \a b -> BoolVal $ a /= b
        
        applyOpN NumLt = applyOp2' $ \a b -> fromBool $ (<) <$> toNum a <*> toNum b
        applyOpN NumLe = applyOp2' $ \a b -> fromBool $ (<=) <$> toNum a <*> toNum b
        applyOpN NumGt = applyOp2' $ \a b -> fromBool $ (>) <$> toNum a <*> toNum b
        applyOpN NumGe = applyOp2' $ \a b -> fromBool $ (>=) <$> toNum a <*> toNum b
        
        applyOp2 :: (Val -> Val -> Val) -> [Val] -> Val
        applyOp2 f = apply
          where
            apply [] = VoidVal
            apply (a:[]) = VoidVal
            apply (a:b:r) = apply' (f a b) r
            
            apply' x [] = x
            apply' x (y:r) = apply' (f x y) r

        applyOp2' :: (Val -> Val -> Val) -> [Val] -> Val
        applyOp2' f = apply
          where
            apply [] = VoidVal
            apply (a:[]) = VoidVal
            apply (a:b:[]) = f a b
            apply r = applyOpN BoolAnd $ apply' r
            
            apply' (a:b:[]) = f a b : []
            apply' (a:r@(b:_)) = f a b : apply' r
        
        applyConds [] = pure VoidVal
        applyConds (CondThen {condWhen = wexp, condThen = texp}:rest) =
          ternary <$> (toBool <$> applyExp wexp) <*> applyExp texp <*> applyConds rest
          where
            ternary (Just True) a _ = a
            ternary _ _ b = b
        applyConds (CondElse {condElse = exp}:_) = applyExp exp
