module Linko.Model.Aeson.TH where

import Data.Char (toLower, isLower)
import Data.Aeson.TH

options = Options { fieldLabelModifier = map toLower . dropWhile isLower
                  , constructorTagModifier = map toLower
                  , allNullaryToStringTag = True
                  , omitNothingFields = True
                  , sumEncoding = ObjectWithSingleField
                  }
