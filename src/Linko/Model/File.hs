module Linko.Model.File (useFile) where

import System.FilePath (FilePath, takeFileName, splitExtension)

import Data.Char (toLower)
import Control.Applicative ((<$>))
import Control.Monad.Trans.Resource (runResourceT)

import Data.Conduit ((=$=), ($$), runConduit, await, yield, awaitForever)
import Data.Conduit.Binary (sourceFile, sinkFile)

import qualified Data.ByteString as B
import Data.ByteString.Lazy (toStrict)
import qualified Data.Conduit.Zlib as Z

import qualified Data.Aeson as J
import qualified Data.Yaml as Y

import Linko.Model
import Linko.Model.Aeson
import Linko.Engine

useFile :: FilePath -> ModelSync
useFile path = ModelSync (load path) (save path)

load :: FilePath -> IO (Either String Model)
load path = runResourceT $ runConduit $ src $$ dec name
  where
    name = toLower <$> takeFileName path
    
    src = sourceFile path
    
    unzip = Z.decompress Z.defaultWindowBits
    
    getAll init done = await >>= maybe (return $ done $ B.concat $ reverse init) (\next -> getAll (next : init) done)
    
    dec path = case splitExtension path of
                (path', ".gz") -> Z.ungzip =$= dec path'
                (path', ".z") -> unzip =$= dec path'
                (_, ".json") -> getAll [] J.eitherDecodeStrict
                (_, ".yaml") -> getAll [] Y.decodeEither
                (path', _) -> awaitForever yield =$= dec path'

save :: FilePath -> Model -> IO ()
save path model = runResourceT $ runConduit $ enc name $$ dst
  where
    name = toLower <$> takeFileName path
    
    dst = sinkFile path
    
    zip = Z.compress 9 Z.defaultWindowBits
    
    enc path = case splitExtension path of
                (path', ".gz") -> enc path' =$= Z.gzip
                (path', ".z") -> enc path' =$= zip
                (_, ".json") -> yield $ toStrict $ J.encode model
