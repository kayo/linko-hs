module Linko.Model.Process where

import Linko.Model

import Data.Default (Default(..))
import qualified Data.Text as T
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as H
import Data.HashSet (HashSet)
import qualified Data.HashSet as S
import Control.Applicative ((<$>), (<*>))

type DepsSet = HashSet Path
type DepsMap = HashMap Path DepsSet

expDeps :: Exp -> DepsSet -> DepsSet
expDeps = recurse
  where
    recurse (Node path) = S.insert path
    recurse (Op1 _ exp) = recurse exp
    recurse (OpN _ exps) = flip (foldr recurse) exps
    recurse (Conds conds) = flip (foldr recurseConds) conds
    recurse _ = id
    
    recurseConds (CondThen wexp texp) = recurse wexp . recurse texp
    recurseConds (CondElse exp) = recurse exp

buildDeps :: Model -> DepsMap
buildDeps = foldr (recurse []) H.empty . H.toList . modelNode
  where
    recurse path (name, (Group {groupNode = s})) = flip (foldr $ recurse (name : path)) $ H.toList s
    recurse path (name, (Entry {entryExp = e})) = H.insert (T.intercalate "." $ reverse $ name : path) ((maybe id expDeps e) S.empty)

unresDeps :: DepsMap -> DepsSet
unresDeps = S.difference <$> S.unions . H.elems <*> S.fromList . H.keys

cycleDeps :: DepsMap -> DepsSet
cycleDeps m = H.foldrWithKey each S.empty m
  where
    each k = flip $ S.foldr (deep k)
    deep k d | k == d = S.insert k
             | otherwise = maybe id (each k) $ d `H.lookup` m

data DepsRes = DepsRes { depsUnres :: DepsSet
                       , depsCycle :: DepsSet
                       }
               deriving (Show, Eq)

instance Default DepsSet where
  def = S.empty

instance Default DepsRes where
  def = DepsRes def def

depsError :: DepsRes -> String
depsError (DepsRes unres cycle) =
  (if unres == S.empty then id else (("Unresolved: " ++ T.unpack (T.intercalate ", " $ S.toList unres)) ++))
  (if cycle == S.empty then "" else "Cyclic: " ++ T.unpack (T.intercalate ", " $ S.toList cycle))

checkDeps :: DepsMap -> DepsRes
checkDeps = DepsRes <$> unresDeps <*> cycleDeps

getModelNode :: Model -> Path -> Maybe Node
getModelNode m p = nav (T.splitOn "." p) (modelNode m)
  where
    nav [] _ = Nothing
    nav (n:[]) m = H.lookup n m
    nav (n:r) m = maybe Nothing (nav r . groupNode) $ H.lookup n m
