module Linko.Model.Aeson () where

import Data.Text (intercalate, splitOn, unpack)
import Data.Aeson
import Data.Aeson.TH
import Data.Default (Default(..))
import Linko.Model
import Control.Applicative ((<$>), (<*>))
import Linko.Model.Aeson.TH
import Data.List (dropWhileEnd)
import Data.Char (toLower, isLower)
import Data.HashMap.Strict (HashMap, member, empty, size, toList)

getDef f v = v .:? f .!= def
putDef f v = if v == def then id else ((f .= v) :)

$(deriveJSON options ''Role)
$(deriveJSON options ''Access)
$(deriveJSON options ''Model)

-- $(deriveJSON options ''Node)

isGroup v = "node" `member` v && not ("type" `member` v || "unit" `member` v)

instance FromJSON Node where
  parseJSON (Object v) = if isGroup v
                         then Group <$>
                              (getDef "title" v) <*>
                              (getDef "description" v) <*>
                              (getDef "access" v) <*>
                              (getDef "node" v)
                         else Entry <$>
                              (getDef "title" v) <*>
                              (getDef "description" v) <*>
                              (getDef "access" v) <*>
                              (getDef "unit" v) <*>
                              (getDef "type" v) <*>
                              (getDef "get" v) <*>
                              (getDef "set" v) <*>
                              (getDef "val" v) <*>
                              (getDef "exp" v)
  parseJSON _ = fail "Node must be an object."

instance ToJSON Node where
  toJSON (Group title description access node) =
    object $
      putDef "title" title $
      putDef "description" description $
      putDef "access" access $
      putDef "node" node
      []
  toJSON (Entry title description access unit type' get set val exp) =
    object $
      putDef "title" title $
      putDef "description" description $
      putDef "access" access $
      putDef "unit" unit $
      putDef "type" type' $
      putDef "get" get $
      putDef "set" set $
      putDef "val" val $
      putDef "exp" exp $
      []

-- $(deriveJSON options { constructorTagModifier = map toLower . init . dropWhileEnd isLower } ''Type)

instance FromJSON Type where
  parseJSON (String "num") = return $ NumType def def def def
  parseJSON (String "str") = return $ StrType def def def
  parseJSON (String "bool") = return $ BoolType def
  parseJSON (String "enum") = return $ EnumType def def
  parseJSON (Object v) = case toList v of
                          (k, (Object v)):[] -> parseField k v
                          _ -> fail "Entry type must be a string or object."
    where
      parseField "num" v = NumType <$>
                           (getDef "def" v) <*>
                           (getDef "min" v) <*>
                           (getDef "max" v) <*>
                           (getDef "stp" v)
      parseField "str" v = StrType <$>
                           (getDef "def" v) <*>
                           (getDef "len" v) <*>
                           (getDef "pat" v)
      parseField "bool" v = BoolType <$>
                            (getDef "def" v)
      parseField "enum" v = EnumType <$>
                            (getDef "def" v) <*>
                            (getDef "val" v)
      parseField _ _ = fail "Invalid entry type object."

instance ToJSON Type where
  toJSON (NumType Nothing Nothing Nothing Nothing) = String "num"
  toJSON (NumType def' min max stp) = object ["num" .= (object $
                                                        putDef "def" def' $
                                                        putDef "min" min $
                                                        putDef "max" max $
                                                        putDef "stp" stp $
                                                        [])]
  toJSON (StrType Nothing Nothing Nothing) = String "str"
  toJSON (StrType def' len pat) = object ["str" .= (object $
                                                    putDef "def" def' $
                                                    putDef "len" len $
                                                    putDef "pat" pat $
                                                    [])]
  toJSON (BoolType Nothing) = String "bool"
  toJSON (BoolType def') = object ["bool" .= (object $
                                              putDef "def" def' $
                                              [])]
  toJSON (EnumType def' val) | def' == def && val == def = String "enum"
                             | otherwise = object ["enum" .= (object $
                                                              putDef "def" def' $
                                                              putDef "val" val $
                                                              [])]

$(deriveJSON options ''EnumOpt)

-- $(deriveJSON options ''Val)

instance FromJSON Val where
  parseJSON (Bool v) = return $ BoolVal v
  parseJSON (Number v) = return $ NumVal v
  parseJSON (String v) = return $ StrVal v
  parseJSON _ = fail "Invalid value."

instance ToJSON Val where
  toJSON (BoolVal v) = Bool v
  toJSON (NumVal v) = Number v
  toJSON (StrVal v) = String v

{-
$(deriveJSON options ''Exp)
$(deriveJSON options ''Unary)
$(deriveJSON options ''Binary)
$(deriveJSON options ''Ternary)
-}

-- $(deriveJSON options ''CondCase)

instance FromJSON CondCase where
  parseJSON (Object v) | "else" `member` v = CondElse <$> v .: "else"
                       | "when" `member` v && "then" `member` v = CondThen <$> v .: "when" <*> v .: "then"
                       | otherwise = fail "Invalid cond case."
  parseJSON _ = fail "Invalid cond case (object required)."

instance ToJSON CondCase where
  toJSON (CondThen k v) = object [ "when" .= k, "then" .= v ]
  toJSON (CondElse v) = object [ "else" .= v ]

-- $(deriveJSON options { constructorTagModifier = opName } ''Exp)

instance FromJSON Exp where
  parseJSON (Object v) = case toList v of
                          (k, _):[] -> v .: k >>= parseOp k
                          _ -> fail "Invalid operation."
    where
      parseOp "node" (String path) = return $ Node path
      
      parseOp "not" e = Op1 BoolNot <$> parseJSON e
      parseOp "neg" e = Op1 NumNeg <$> parseJSON e
      parseOp "len" e = Op1 StrLen <$> parseJSON e
      
      parseOp "eq" e = OpN Equal <$> parseJSON e
      parseOp "neq" e = OpN NotEq <$> parseJSON e
      
      parseOp "and" e = OpN BoolAnd <$> parseJSON e
      parseOp "or" e = OpN BoolOr <$> parseJSON e
      
      parseOp "add" e = OpN NumAdd <$> parseJSON e
      parseOp "sub" e = OpN NumSub <$> parseJSON e
      parseOp "mul" e = OpN NumMul <$> parseJSON e
      parseOp "div" e = OpN NumDiv <$> parseJSON e

      parseOp "lt" e = OpN NumLt <$> parseJSON e
      parseOp "le" e = OpN NumLe <$> parseJSON e
      parseOp "gt" e = OpN NumGt <$> parseJSON e
      parseOp "ge" e = OpN NumGe <$> parseJSON e
      
      parseOp "cat" e = OpN StrCat <$> parseJSON e
      
      parseOp "cond" e = Conds <$> parseJSON e
      
      parseOp op _ = fail $ "Invalid operation: " ++ unpack op
  
  parseJSON v = Val <$> parseJSON v

instance ToJSON Exp where
  toJSON (Val v) = toJSON v
  toJSON (Node v) = object ["node" .= v]
  
  toJSON (Op1 BoolNot v) = object ["not" .= v]
  toJSON (Op1 NumNeg v) = object ["neg" .= v]
  toJSON (Op1 StrLen v) = object ["len" .= v]
  
  toJSON (OpN Equal v) = object ["eq" .= v]
  toJSON (OpN NotEq v) = object ["neq" .= v]
  
  toJSON (OpN BoolAnd v) = object ["and" .= v]
  toJSON (OpN BoolOr v) = object ["or" .= v]
  
  toJSON (OpN NumAdd v) = object ["add" .= v]
  toJSON (OpN NumSub v) = object ["sub" .= v]
  toJSON (OpN NumMul v) = object ["mul" .= v]
  toJSON (OpN NumDiv v) = object ["div" .= v]

  toJSON (OpN NumLt v) = object ["lt" .= v]
  toJSON (OpN NumLe v) = object ["le" .= v]
  toJSON (OpN NumGt v) = object ["gt" .= v]
  toJSON (OpN NumGe v) = object ["ge" .= v]
  
  toJSON (OpN StrCat v) = object ["cat" .= v]
  
  toJSON (Conds v) = object ["cond" .= v]
