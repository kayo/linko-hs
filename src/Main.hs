module Main (main) where

--import Data.Text (Text)
import Linko.Model
import Linko.Model.File (useFile)
import Linko.Engine (newEngine, runEngine, loadModel, getModel, getValue, setValue, onValue, offValue)
import Data.ByteString.Char8 (unpack)
--import Data.Aeson (decode, encode)
import Data.Yaml (decode, encode)

import Control.Applicative
import Control.Monad.IO.Class (liftIO)
import FRP.Sodium

main :: IO ()
main = do
  engine <- newEngine $ useFile "config.yaml.gz"
  res <- flip runEngine engine $ do
    loadModel
    --model <- getModel
    --liftIO $ putStrLn $ show model
    let onChange = onValue "self"
    --let offChange = offValue "self"
    
    onChange "time.hour" $ \v -> putStrLn $ "time.hour=" ++ show v
    onChange "state.tariff" $ \v -> putStrLn $ "state.tariff=" ++ show v
    onChange "state.test" $ \v -> putStrLn $ "state.test=" ++ show v
    
    --v <- (,) <$> getValue "time.hour" <*> getValue "state.tariff"
    --liftIO $ putStrLn $ show v
    
    setValue "time.hour" $ NumVal $ fromInteger 11
    
    --v <- (,) <$> getValue "time.hour" <*> getValue "state.tariff"
    --liftIO $ putStrLn $ show v
    setValue "time.hour" $ NumVal $ fromInteger 23
  either fail return res
  return ()
